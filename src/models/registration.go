package models

import "web-api/utils/database"

type Users struct {
	ID       uint   `gorm:"primary_key" json:"id" `
	Email    string `gorm:"type:varchar(150);" json:"email" valid:"length(3|150)"`
	Phone    string `gorm:"type:mediumtext" json:"phone" valid:"minstringlength(10)"`
	UserName string `gorm:"type:mediumtext" json:"user_name"`
	FullName string `gorm:"type:mediumtext" json:"full_name"`
	Password string `gorm:"type:mediumtext" json:"password" valid:"required,minstringlength(4)"`
	Token    string `gorm:"type:varchar(150);" json:"token"`
	Role     int    `gorm:"type:int"  json:"role"`
	OTP      string `gorm:"type:mediumtext" json:"otp"`
	Status   int    `gorm:"default:0" json:"status"`
}

// type ClientsList struct {
// 	Id           int    `gorm:"column:id; PRIMARY_KEY" json:"id"`
// 	Client_Name  string `gorm:"column:client_name;type:varchar(255);" json:"client_name"`
// 	Company_name string `gorm:"column:company_name;type:varchar(255);" json:"company_name"`
// 	Country      string `gorm:"column:country;type:varchar(255);" json:"country"`
// 	Currency     string `gorm:"column:currency;type:varchar(255);" json:"currency"`
// 	CreatedAt    string `gorm:"column:created_at;type:varchar(255);" json:"created_at,omitempty"`
// 	UpdatedAt    string `gorm:"column:updated_at;type:varchar(255);" json:"updated_at,omitempty"`
// 	// DeletedAt    string `gorm:"column:deleted_at;type:varchar(255);null" json:"deleted_at"`
// }
type Pagination struct {
	Result       interface{} `json:"result"`
	CurrentCount int         `json:"currentCount"`
	Total        int         `json:"total"`
	TotalPages   int         `json:"total_pages"`
}

func UsersMigrate() {
	database.DB.Debug().AutoMigrate(&Users{})
}
