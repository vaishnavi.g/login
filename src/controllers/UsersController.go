package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"web-api/src/models"
	"web-api/src/service"
	"web-api/utils/constant"
	util "web-api/utils/password"
	"web-api/utils/response"
	res "web-api/utils/response"
	val "web-api/utils/validator"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

func CreateUserDetails(c *gin.Context) {

	reqModel := models.Users{}
	if err := c.ShouldBind(&reqModel); err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorMessage(constant.BADREQUEST, err))
		return
	}
	if err := val.Validate(reqModel); err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorMessage(constant.BADREQUEST, err))
		return
	}
	hashPass, err := util.HashPassword(reqModel.Password)
	reqModel.Password = hashPass
	fmt.Println(hashPass)
	fmt.Println(reqModel.Password)
	var service = service.UserService{}
	count, err := service.GetEmail(reqModel.Email)
	if err != nil {
		c.JSON(http.StatusPreconditionFailed, res.PreConditionFailed(c, err))
		return
	}
	if count > 0 {
		c.JSON(http.StatusPreconditionFailed, res.PreConditionFailed(c, res.CustomError("Email is already Registered")))
		return
	}
	reqModel.OTP, err = util.GenerateOTP(6)
	saved, err := service.CreateUserDetails(&reqModel)
	if err != nil {
		log.Error().Msgf("Error inserting data into the database: %s", err.Error())
		c.JSON(http.StatusInternalServerError, response.ErrorMessage(constant.INTERNALSERVERERROR, err))
		return
	}
	log.Info().Msgf("User Inserted Succesfully in the database: %s", saved)
	c.JSON(http.StatusOK, response.SuccessResponse(saved))

}

func ReadUsers(c *gin.Context) {
	var (
		u   models.Users
		err error
	)
	c.BindJSON(&u)
	var service = service.UserService{}
	readUsers, err := service.FetchUserDetails(&u)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"apiResponse": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"apiResponse": readUsers})
}

func DeleteUser(c *gin.Context) {

	delete := c.Param("id")
	Delete, _ := strconv.Atoi(delete)
	testslackService := service.UserService{}
	message, err := testslackService.DeleteUsers(Delete)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"apiResponse": err})
	} else {
		fmt.Println(message)
		c.JSON(http.StatusOK, gin.H{"apiResponse": message})
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func LoginUser(c *gin.Context) {
	var user models.Users
	Service := service.UserService{}

	c.BindJSON(&user)
	fmt.Println("user", user)
	hashPass, err := util.HashPassword(user.Password)
	user.Password = hashPass

	count, err := Service.CheckLogin(user.Email, user.Password)
	if count == 0 {
		c.JSON(http.StatusPreconditionFailed, res.PreConditionFailed(c, res.CustomError("Invalid Credential")))
		return
	}
	if err != nil {
		c.JSON(http.StatusPreconditionFailed, res.PreConditionFailed(c, err))
		return
	}
	token := util.TokenGenerator(64)

	user.Token = token //assigning the value (no need to bind from server)
	// service.UpdateToken(token, user.Email)

	outResponse := map[string]interface{}{"message": "Login successfully", "token": token}
	c.JSON(http.StatusOK, res.SuccessResponses(c, outResponse))
}
func UpdateUser(c *gin.Context) {
	edit := models.Users{}

	if c.ShouldBind(&edit) == nil {
		log.Info().Msg("binding successful for params")
	}
	testslackService := service.UserService{}
	message, model, err := testslackService.UpdateUserDetails(edit)
	hashPass, err := util.HashPassword(model.Password)
	edit.Password = hashPass
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"apiResponse": err})
	} else {
		fmt.Println(message)
		c.JSON(http.StatusOK, gin.H{"apiResponse": message})
	}
}
func VerifyRegisterOTP(c *gin.Context) {
	var u models.Users
	c.BindJSON(&u)
	Service := service.UserService{}
	t := u.OTP
	count, err := Service.VerifyRegisterOTP(t, u.Email)
	if count == 0 {
		c.JSON(http.StatusPreconditionFailed, res.PreConditionFailed(c, res.CustomError("Invalid OTP")))
		return
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, res.InternalServerError(c, res.CustomError("Internal server error")))
		return
	}

	err = Service.UpdateUserStatus(u.Email, 1)
	if err != nil {
		c.JSON(http.StatusInternalServerError, res.InternalServerError(c, res.CustomError("Internal server error")))
		return
	}

	outResponse := map[string]interface{}{"message": "Registered"}
	c.JSON(http.StatusOK, res.SuccessResponse(outResponse))
}
