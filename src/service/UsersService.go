package service

import (
	"web-api/src/models"
	"web-api/src/repository"
	"web-api/utils/database"
)

type UserService struct{}

func (c *UserService) CreateUserDetails(insert *models.Users) (string, error) {

	if err := repository.Repo.Insert(insert); err != nil {
		return "Unable to insert user", err
	}

	return "User inserted sucessfully", nil
}

func (c *UserService) FetchUserDetails(read *models.Users) (models.Users, error) {
	var user models.Users
	database.DB.Debug().Where("id = ? ", read.ID).Find(&user)
	return user, nil
}

func (i *UserService) GetEmail(ID string) (int, error) {
	//database connection

	var Count int
	database.DB.Debug().Select(`id,email,password,phone,token,full_name,role,user_name`).
		Where(`email=?`, ID).Table("users").Scan(&i).Count(&Count)

	return Count, nil
}

func (c *UserService) DeleteUsers(Delete int) (string, error) {
	var delete models.Users
	if err := database.DB.Where("id=?", Delete).Delete(&delete).Error; err != nil {
		return "Unable to delete ", err
	}
	return "Delete sucessfully", nil
}

func (c *UserService) CheckLogin(Email string, Password string) (int, error) {
	//database connection
	var Count int
	var u models.Users
	database.DB.Debug().Select("*").Where(`Email=?`, Email).Where(`password=?`, Password).Table("users").Count(&Count).Scan(&u)

	return Count, nil
}
func (c *UserService) UpdateToken(token string, email string) error {
	//database connection
	var u models.Users
	err := database.DB.Debug().Model(&u).Where("email = ? ", email).Updates(
		map[string]interface{}{
			"token": token,
		}).Error

	if err != nil {
		return err
	}
	return nil
}
func (c *UserService) UpdateUserDetails(update models.Users) (string, models.Users, error) {

	if err := database.DB.Model(&models.Users{}).Where("id = ?", update.ID).Updates(map[string]interface{}{
		"user_name": update.UserName,
		"full_name": update.FullName,
		"password":  update.Password,
		"role":      update.Role,
		"status":    update.Status,
		"email":     update.Email}).Error; err != nil {
		return "Unable to update", models.Users{}, err
	}
	return "Updated sucessfully", models.Users{}, nil
}
func (c *UserService) VerifyRegisterOTP(otp string, email string) (int, error) {
	//database connection
	var user models.Users
	var Count int
	database.DB.Debug().Select(`id,email,password,phone,token,full_name,role,user_name,otp`).
		Where(`Email=?`, user.Email).Where(`otp=?`, user.OTP).Table(`users`).Count(&Count)
	return Count, nil
}
func (c *UserService) UpdateUserStatus(email string, status int) error {
	//database connection

	err := database.DB.Debug().Table("users").Where("email = ? ", email).Updates(
		map[string]interface{}{
			"status": status,
		}).Error
	if err != nil {
		return err
	}
	return err
}
