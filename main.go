package main

import (

	// "web-api/config"

	config "web-api/config"
	"web-api/migration"
	"web-api/route"
	"web-api/src/repository"
	"web-api/utils/database"
	logger "web-api/utils/logging"
	"web-api/utils/validator"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"
)

func main() {

	config.LoadConfig()
	// viper.SetConfigFile(".env")
	router := gin.Default()
	logger.NewLogger(viper.GetString("logging.level"))
	database.GetInstancemysql()
	migration.Migration()
	repository.MySqlInit()
	validator.Init()

	// fmt.Sprintln(data)

	// router.Use(middleware.TracingMiddleware())
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowMethods = []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"}
	corsConfig.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization", "Request-With"}
	router.Use(cors.New(corsConfig))
	route.SetupRoutes(router)

}
