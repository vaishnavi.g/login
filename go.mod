module web-api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.11.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.5
	github.com/jung-kurt/gofpdf v1.16.2
	github.com/rimiti/kill-port v1.0.0 // indirect
	github.com/robfig/cron v1.2.0
	github.com/rs/zerolog v1.26.1
	github.com/shomali11/slacker v0.0.0-20220129203130-6c28a41fb7b0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.11.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
