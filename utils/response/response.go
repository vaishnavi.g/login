package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Success struct {
	Code   int         `json:"code"`
	Status bool        `json:"status"`
	Data   interface{} `json:"data"`
}

//Response -
type Response struct {
	Code   int         `json:"code"`
	Status bool        `json:"status"`
	Data   interface{} `json:"data"`
}

//Error -  common json error
type Error struct {
	Status      bool   `json:"status"`
	Code        int    `json:"code"`
	Description string `json:"error"`
	trace       string
	Error       string `json:"error"`
}
type errorString struct {
	s string
}

func ErrorMessage(code int, e error) Error {

	errMsg := Error{
		Status: false,
		Code:   code,
		Error:  e.Error(),
	}

	return errMsg
}

//SuccessResponse -
func SuccessResponse(data interface{}) Response {
	response := Response{
		Code:   http.StatusOK,
		Status: true,
		Data:   data,
	}
	return response
}
func SuccessResponses(c *gin.Context, data interface{}) Response {
	response := Response{
		Code:   http.StatusOK,
		Status: true,
		Data:   data,
	}
	return response
}

func (e *errorString) Error() string {
	return e.s
}

//CustomError for error handling
func CustomError(text string) error {
	return &errorString{text}
}

//InternalServerError - internal server error
func InternalServerError(c *gin.Context, e error) Error {

	errMsg := Error{
		Status:      false,
		Code:        http.StatusInternalServerError,
		Description: e.Error(),
	}

	return errMsg
}

//BadDataRequest - validator response
func BadDataRequest(c *gin.Context, e error) Error {

	errMsg := Error{
		Status:      false,
		Code:        http.StatusBadRequest,
		Description: e.Error(),
	}

	return errMsg
}

//PreConditionFailed - validator response
func PreConditionFailed(c *gin.Context, e error) Error {

	errMsg := Error{
		Status:      false,
		Code:        http.StatusPreconditionFailed,
		Description: e.Error(),
	}

	return errMsg
}

//Unauthorised - validator response
func Unauthorised(c *gin.Context, e error) Error {

	errMsg := Error{
		Status:      false,
		Code:        http.StatusUnauthorized,
		Description: e.Error(),
	}

	return errMsg
}

//Forbidden - validator response
func Forbidden(c *gin.Context, e error) Error {

	errMsg := Error{
		Status:      false,
		Code:        http.StatusForbidden,
		Description: e.Error(),
	}

	return errMsg
}
