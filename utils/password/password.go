package util

import (
	"crypto/rand"
	"crypto/sha256"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	data := []byte(password)
	hash := sha256.Sum256(data)
	return fmt.Sprintf("%x", hash[:]), nil

}
func CheckPassword(password string, hashedpassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedpassword), []byte(password))
}

const otpChars = "1234567890"

func TokenGenerator(bytevalue int) string {
	b := make([]byte, bytevalue)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func GenerateOTP(length int) (string, error) {

	buffer := make([]byte, length)
	_, err := rand.Read(buffer)
	if err != nil {
		return "", err
	}

	otpCharsLength := len(otpChars)
	for i := 0; i < length; i++ {
		buffer[i] = otpChars[int(buffer[i])%otpCharsLength]
	}

	return string(buffer), nil
}
