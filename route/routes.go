package route

import (
	"web-api/src/controllers"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func SetupRoutes(router *gin.Engine) {

	api := router.Group("MocktestAPI")
	//clientAPI- routes
	api.POST("/create/list", controllers.CreateUserDetails)
	api.GET("/user/details", controllers.ReadUsers)
	api.DELETE("/delete/user/:id", controllers.DeleteUser)
	api.POST("/verify/otp", controllers.VerifyRegisterOTP)
	api.POST("/create/login/list", controllers.LoginUser)
	api.PUT("/update/user/details", controllers.UpdateUser)

	router.Run(viper.GetString("server.port"))
}
